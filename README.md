# CBTAndroid
An Android application to help promote better mental health
## More Info
https://copoer.gitlab.io/CBTAndroid

[<img src="public/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.theworld.help.cbtandroid/)
## Features thats would be cool
- Entry Import and Export
- Gratitude list
